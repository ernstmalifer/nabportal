'use strict';

/**
 * @ngdoc function
 * @name nabportalApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the nabportalApp
 */
angular.module('nabportalApp')
  .controller('AboutCtrl', function ($scope, $routeParams) {

    $scope.id = $routeParams.id;

  });
