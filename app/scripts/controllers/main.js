'use strict';

/**
 * @ngdoc function
 * @name nabportalApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nabportalApp
 */
angular.module('nabportalApp')
  .controller('MainCtrl', function ($scope, $location) {
    $scope.proceed = function(code){
        $location.path( '/about/' + code );
    };
  });
